# utils.py
import pygame

def load_image(image_path):
    """Load an image from a given file path."""
    image = pygame.image.load(image_path)
    return image

def load_sound(sound_path):
    """Load a sound from a given file path."""
    sound = pygame.mixer.Sound(sound_path)
    return sound