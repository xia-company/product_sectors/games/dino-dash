# settings.py

class Settings:
    """Class to store game settings."""
    def __init__(self, screen_width, screen_height, player_image, obstacle_image):
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.player_image = player_image
        self.obstacle_image = obstacle_image

    def load_from_file(self, filename):
        """Load game settings from a file."""
        # Implementation for loading settings from a file
        pass

    def save_to_file(self, filename):
        """Save game settings to a file."""
        # Implementation for saving settings to a file
        pass