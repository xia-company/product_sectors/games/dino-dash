# obstacle.py

class Obstacle:
    """Class to represent an obstacle in the game."""
    def __init__(self, image, position, velocity, speed):
        self.image = image
        self.position = position
        self.velocity = velocity
        self.speed = speed

    def update(self, dt):
        """Update the obstacle state based on the time since the last update."""
        # Implementation for updating obstacle state

    def render(self, screen):
        """Render the obstacle on the game screen."""
        # Implementation for rendering obstacle on the screen