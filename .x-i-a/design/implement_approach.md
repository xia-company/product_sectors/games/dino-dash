To design a SOTA (State-of-the-Art) PEP8-compliant Python system for the "Dino Dash" game, we will use the following open-source frameworks and tools:

- Pygame: Pygame is a popular and widely-used library for game development in Python. It provides functionality for handling graphics, animations, and user input, making it an ideal choice for creating an immersive gaming experience.
- Pygame Zero: Pygame Zero is a simple and lightweight framework built on top of Pygame. It provides a simplified API for game development, making it easier to create games without the need for boilerplate code.
- Pylint: Pylint is a static code analysis tool that helps ensure code quality and adherence to PEP8 standards. By integrating pylint into our development workflow, we can catch and fix issues related to code style, potential bugs, and other code quality concerns.

Using Pygame and Pygame Zero will enable us to create captivating gameplay mechanics, smooth controls, and visually appealing graphics and animations. Pylint will help us maintain a high level of code quality and readability throughout the development process.