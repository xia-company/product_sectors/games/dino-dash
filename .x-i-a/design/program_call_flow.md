```mermaid
sequenceDiagram
    participant MainApp as Main Application
    participant Game as Game Instance
    participant Player as Player Instance
    participant Obstacle as Obstacle Instance
    participant Utils as Utility Functions
    ...
    MainApp->Game: start
    Game->Player: __init__
    Game->MainApp: update
    MainApp->Game: handle_input
    Game->Player: update
    Game->Obstacle: update
    MainApp->Game: render
    Game->Player: render
    Game->Obstacle: render
    Game->MainApp: handle_input
    MainApp->Utils: load_image
    MainApp->Utils: load_sound
    ...
```