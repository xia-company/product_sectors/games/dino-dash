```python
[
    "Utils provides utility functions such as 'load_image' and 'load_sound'.",
    "Settings stores game settings like screen width and height.",
    "Player represents the player character in the game.",
    "Obstacle represents an obstacle that the player must avoid.",
    "Game handles the game logic and controls the player and obstacles.",
    "MainApp is the main application that starts and runs the game.",
]
```