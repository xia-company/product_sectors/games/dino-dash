```mermaid
classDiagram
    class Player {
        - image: str
        - position: Tuple[int, int]
        - velocity: Tuple[int, int]
        - speed: int
        + __init__(self, image: str)
        + update(self, dt: float)
        + render(self, screen: pygame.Surface)
        + handle_input(self, event: pygame.event.EventType)
    }
    
    class Obstacle {
        - image: str
        - position: Tuple[int, int]
        - velocity: Tuple[int, int]
        - speed: int
        + __init__(self, image: str, position: Tuple[int, int], velocity: Tuple[int, int], speed: int)
        + update(self, dt: float)
        + render(self, screen: pygame.Surface)
    }
    
    class Game {
        - screen: pygame.Surface
        - player: Player
        - obstacles: List[Obstacle]
        - score: int
        + __init__(self, screen_width: int, screen_height: int)
        + start(self)
        + update(self, dt: float)
        + render(self)
        + handle_input(self)
    }
    
    class Settings {
        - screen_width: int
        - screen_height: int
        - player_image: str
        - obstacle_image: str
        ...
        + load_from_file(self, filename: str)
        + save_to_file(self, filename: str)
    }
    
    class MainApp {
        - game: Game
        - settings: Settings
        + __init__(self)
        + run(self)
    }
    
    class Utils {
        + load_image(image_path: str) -> pygame.Surface
        + load_sound(sound_path: str) -> pygame.mixer.Sound
    }
    
    Game "1" *-- "1" Player: has
    Game "1" o-- "n" Obstacle: has
    MainApp "1" o-- "1" Game: has
    MainApp "1" o-- "1" Settings: has
    Player <|--- Obstacle
    Player <|-- Game
    Game <|-- MainApp
```